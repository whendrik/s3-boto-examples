# S3 Boto examples

Explanation and Examples for usage of [S3 Boto Client in Python](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html#client)


## Notebook Environment

Notebooks run in their own container. The container is initialized with a standard filesystem of the linux base. Project Files are not visible/mounted.

`S3` Objects/Files, can be be used in notebooks by

1. [Directly read them into Panda's or Spark Dataframes](https://dataplatform.cloud.ibm.com/docs/content/wsj/analyze-data/load-and-access-data.html).
2. Download them, to the containers local filesystem.

Method (2) is described here, together with examples how to upload data back.

### Examples of Using the S3 Client - Download / Upload to local filesystem

Say `client_01fa29576c1949bf8bf19f9e93a1ba4a` is a [S3 Boto Client Object](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html#client)

```
client_01fa29576c1949bf8bf19f9e93a1ba4a = ibm_boto3.client(service_name='s3',
    ibm_api_key_id='XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
    ibm_auth_endpoint="https://iam.bluemix.net/oidc/token",
    config=Config(signature_version='oauth'),
    endpoint_url='https://s3-api.us-geo.objectstorage.service.networklayer.com')
```

We can make use of [upload_file()](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html#S3.Client.upload_file), [put_object()](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html#S3.Client.put_object), and [download_file()](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html#S3.Client.download_file) to transfer files from/to `S3` to the local containers filesystem.

### Download a file to local filesystem

Say `Bucket='some-buckey-hu4dz19vtz'` contains `Key='file.csv'`, than we can download that file to local filesystem with

```
client_01fa29576c1949bf8bf19f9e93a1ba4a.download_file(Bucket='some-buckey-hu4dz19vtz', Key='file.csv', Filename='file.csv')
```

If success, the file is visible with

```
!ls
```

And will show `file.csv`

Sometimes, the filename is not known, e.g. it was created by an automated process. Use [list_objects()](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html#S3.Client.list_objects) or [list_objects_v2()](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html#S3.Client.list_objects_v2) to see the content of the Bucket before deciding what to download.

### Upload a file to S3 Bucket

Say `local_file.csv` is a file we want to put in `Bucket='some-buckey-hu4dz19vtz'`. Maybe the file is a csv output of a dataframe, e.g. `df.to_csv("local_file.csv")`.
We can upload the csv file back in a bucket with

```
client_01fa29576c1949bf8bf19f9e93a1ba4a.upload_file("local_file.csv", Bucket='some-buckey-hu4dz19vtz', Key="local_file.csv")
```